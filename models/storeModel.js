var mongoose = require('mongoose');

var storeSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    }
});

var Store = module.exports = mongoose.model('Store', storeSchema);
module.exports.get = function (callback, limit) {
    Store.find(callback).limit(limit);
}