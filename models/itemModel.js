var mongoose = require('mongoose');

var itemSchema = mongoose.Schema({
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    quantity: {
    	type: Number,
        required: true
    }
});

var Item = module.exports = mongoose.model('Item', itemSchema);
module.exports.get = function (callback, limit) {
    Item.find(callback).limit(limit);
}