var mongoose = require('mongoose');

var Item = require('./itemModel').schema;
var Payment = require('./paymentModel').schema;

var orderSchema = mongoose.Schema({
    address: {
        type: String,
        required: true
    },
    confirmationDate: {
        type: Date,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    items: {
        type: [Item],
        required: true
    },
    payment: {
    	type: Payment
    }
});

var Order = module.exports = mongoose.model('Order', orderSchema);
module.exports.get = function (callback, limit) {
    Order.find(callback).limit(limit);
}