var mongoose = require('mongoose');

var paymentSchema = mongoose.Schema({
    status: {
        type: String,
        required: true
    },
    paymentDate: {
        type: Date,
        required: true
    },
    creditCard: {
        type: Number,
        required: true
    }
});

var Payment = module.exports = mongoose.model('Payment', paymentSchema);
module.exports.get = function (callback, limit) {
    Payment.find(callback).limit(limit);
}