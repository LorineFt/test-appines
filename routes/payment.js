let router = require('express').Router();

Order = require('../models/orderModel');
Payment = require('../models/paymentModel');

router.post('/:id', function (req, res) {
    createPayment(req.body, res, (payment) => {
        Order.findById(req.params.id, function (err, order) {
            if (err)
                res.send(err);
            if (!order)
                res.json({
                    status: "error",
                    message: "This order does not exist"
                });
            else {
                order.payment = payment;
                order.status = "Order in preparation"
                order.save(function (err) {
                    if (err)
                        res.json(err);
                    res.json({
                        message: 'Payment added to order',
                        data: order
                    });
                });
            }
        });
    });
});

function createPayment(paymentInfos, res, callback) {
        var payment = new Payment();
        payment.status = "Confirmed";
        payment.paymentDate = new Date();
        payment.creditCard = paymentInfos.creditCard;
        payment.save(function (err) {
            if (err)
                res.json(err);
            callback(payment);
        });
}

module.exports = router;