let router = require('express').Router();

Order = require('../models/orderModel');
Item = require('../models/itemModel');

router.get('/', function (req, res) {
    Order.get(function (err, orders) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Orders retrieved successfully",
            data: orders
        });
    });
});

router.post('/', function (req, res) {
    if (req.body.items.length == 0)
        res.json({
            status: "error",
            message: "Order must at least have one item"
        });
    createItems(req.body.items, (items) => {
        var order = new Order();
        order.address = req.body.address;
        order.status = "Order received";
        order.confirmationDate = new Date();
        order.items = items;
        order.save(function (err) {
    	    if (err)
    	        res.json(err);
    		res.json({
    	        message: 'New order created!',
    	        data: order
    	    });
    	});
    });
});

router.delete('/:id', function (req, res) {
    Order.remove({ _id: req.params.id }, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Order deleted'
        });
    });
});

function createItems(items, callback) {
    var itemList = [];
    items.forEach(element => {
        var item = new Item();
        item.description = element.description;
        item.price = element.price;
        item.quantity = element.quantity;
        item.save(function (err) {
            if (err)
                res.json(err);
            itemList.push(item);
            if (itemList.length == items.length)
                callback(itemList);
        });
    });
}

module.exports = router;