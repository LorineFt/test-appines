let router = require('express').Router();

Store = require('../models/storeModel');

router.get('/', function (req, res) {
    Store.get(function (err, stores) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Stores retrieved successfully",
            data: stores
        });
    });
});

router.post('/', function (req, res) {
    var store = new Store();
    store.name = req.body.name ? req.body.name : store.name;
    store.address = req.body.address;
    store.save(function (err) {
	    if (err)
	        res.json(err);
		res.json({
	        message: 'New store created!',
	        data: store
	    });
	});
});

router.put('/:id', function (req, res) {
	Store.findById(req.params.id, function (err, store) {
        if (!store)
            res.json({
                status: "error",
                message: 'This store does not exist'
            });
        else {
            if (err)
                res.send(err);
    		store.name = req.body.name ? req.body.name : store.name;
    		store.address = req.body.address ? req.body.address : store.address;
            store.save(function (err) {
                if (err)
                    res.json(err);
                res.json({
                    message: 'Store Info updated',
                    data: store
                });
            });
        }
    });
});

router.delete('/:id', function (req, res) {
    Store.remove({ _id: req.params.id }, function (err, contact) {
        if (err)
            res.send(err);
		res.json({
            status: "success",
            message: 'Store deleted'
        });
    });
});

module.exports = router;