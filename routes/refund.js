let router = require('express').Router();

Order = require('../models/orderModel');

router.put('/', function (req, res) {
    Order.findById(req.body.orderId, function (err, order) {
        if (err)
            res.send(err);
        if (!order)
            res.json({
                status: "error",
                message: "This order does not exist"
            });
        if (order && !order.payment)
            res.json({
                status: "error",
                message: "This order is not paid"
            });
        else {
            if (checkRefund(order.payment.paymentDate, order.payment.status)) {
                order.payment.status = "Refunded";
                order.status = "Order canceled"
                order.save(function (err) {
                    if (err)
                        res.json(err);
                    res.json({
                        message: 'Order canceled and refunded',
                        data: order
                    });
                });
            }
            else {
                res.json({
                    status: "error",
                    message: "You cannot refund this order"
                });
            }
        }
    });
});

router.put('/item', function (req, res) {
    Order.findById(req.body.orderId, function (err, order) {
        if (err)
            res.send(err);
        if (!order)
            res.json({
                status: "error",
                message: "This order does not exist"
            });
        if (order && !order.payment)
            res.json({
                status: "error",
                message: "This order is not paid"
            });
        else {
            var exist = false;
            order.items.forEach((element, index) => {
                if (element._id == req.body.itemId) {
                    exist = true
                    if (checkRefund(order.payment.paymentDate, order.payment.status)) {
                        order.items[index].description = "Item refunded";
                        order.save(function (err) {
                            if (err)
                                res.json(err);
                            res.json({
                                message: 'Item refunded',
                                data: order
                            });
                        });
                    }
                    else {
                        res.json({
                            status: "error",
                            message: "You cannot refund this item"
                        });
                    }
                }
            });
            if (!exist)
                res.json({
                    status: "error",
                    message: "This item is not part of the order"
                });
        }
    });
});

function checkRefund(paymentDate, paymentStatus) {
    var refundDate = new Date();
    refundDate.setDate(refundDate.getDate() - 10);
    if (Date.parse(refundDate) < Date.parse(paymentDate) && paymentStatus == "Confirmed")
        return true;
    else
        return false;
}

module.exports = router;