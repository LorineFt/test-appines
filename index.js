let express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');

let storeRoutes = require("./routes/store")
let orderRoutes = require("./routes/order")
let paymentRoutes = require("./routes/payment")
let refundRoutes = require("./routes/refund")

let app = express();

var port = 8080;

app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/resthub', { useNewUrlParser: true, useUnifiedTopology: true });

var db = mongoose.connection;

app.use('/store', storeRoutes);
app.use('/order', orderRoutes);
app.use('/payment', paymentRoutes);
app.use('/refund', refundRoutes);

app.listen(port, function () {
	console.log("Running on port " + port);
});